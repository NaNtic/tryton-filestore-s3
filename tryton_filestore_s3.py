# This file is part of filestore-s3. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import boto3
import uuid

from trytond.config import config
from trytond.filestore import FileStore

access_key = config.get('database', 'access_key', default='')
secret_key = config.get('database', 'secret_key', default='')
bucket = config.get('database', 'bucket')
endpoint = config.get('database', 'endpoint', default=None)


class FileStoreS3(FileStore):
    _client = None

    def get(self, id, prefix=''):
        key = name(id, prefix)
        response = self.client.get_object(Bucket=bucket, Key=key)
        return response['Body'].read()

    def getmany(self, ids, prefix=''):
        return [self.get(id, prefix) for id in ids]

    def size(self, id, prefix=''):
        key = name(id, prefix)
        response = self.client.head_object(Bucket=bucket, Key=key)
        return response['ContentLength']

    def sizemany(self, ids, prefix=''):
        return [self.size(id, prefix) for id in ids]

    def set(self, data, prefix=''):
        id = uuid.uuid4().hex
        key = name(id, prefix)
        storage_class = config.get('database', 'storage_class',
            default='STANDARD_IA')
        self.client.put_object(Bucket=bucket, Key=key, Body=data,
            StorageClass=storage_class)
        return id

    def setmany(self, data, prefix=''):
        return [self.set(d, prefix) for d in data]

    def list(self, prefix=''):
        # This method is not part of FileStore API
        if prefix:
            prefix += '/'
        response = self.client.list_objects(Bucket=bucket, Prefix=prefix)
        for obj in response['Contents']:
            yield obj['Key']

    def delete(self, id, prefix=''):
        # This method is not part of FileStore API
        key = name(id, prefix)
        self.client.delete_object(Bucket=bucket, Key=key)

    def set_with_id(self, id, data, prefix=''):
        # This method is not part of FileStore API
        key = name(id, prefix)
        storage_class = config.get('database', 'storage_class',
            default='STANDARD_IA')
        self.client.put_object(Bucket=bucket, Key=key, Body=data,
            StorageClass=storage_class)
        return id

    @property
    def client(self):
        # This method is not part of FileStore API
        if FileStoreS3._client is None:
            FileStoreS3._client = boto3.client('s3',
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key,
                endpoint_url=endpoint,
                )
        return FileStoreS3._client

def name(id, prefix=''):
    return '/'.join(filter(None, [prefix, id]))
